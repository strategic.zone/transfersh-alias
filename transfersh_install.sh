#!/usr/bin/env bash
#title		    :transfersh_install.sh
#description	:Install script and add alias for transfersh
#author		    :Valeriu Stinca
#email		    :ts@strategic.zone
#date		    :2021-08-17
#version	    :2
#notes		    :
#===================
git clone https://gitlab.com/strategic.zone/transfersh-alias.git ~/.transfersh
read -r -p "Press Enter for https://tranfer.sh server or your enter your tranfersh server? https://" TS_SERVER
TS_SERVER=${TS_SERVER:-transfer.sh}
sed -i "/^TS_SERVER=/c\TS_SERVER=\"https://${TS_SERVER}\"" ~/.transfersh/transfersh-alias.sh
alias trs="~/.transfersh/transfersh-alias.sh "
echo 'alias trs="~/.transfersh/transfersh-alias.sh "' | tee -a ~/.aliasrc
echo "All is OK! Please check your TS_SERVER in ~/.transfersh/transfersh-alias.sh"

